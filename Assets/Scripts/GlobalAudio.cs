﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalAudio :MonoBehaviour{
	
	public static GameObject globalGameObject;

	AudioSource audiosource;

	//------Methods of temporary created object-----

	void Start(){
		audiosource=GetComponent<AudioSource>();

		if (audiosource.clip != null) {
			audiosource.Play ();
			Destroy (gameObject, audiosource.clip.length + 1.0f);
		}
	}

	void OnDestroy(){
		if (audiosource.clip == null) {
			globalGameObject = null;
		}
	}

	//--------Static Methods----------

	public static void PlayOneShot(AudioClip clip){

		if (globalGameObject == null)
			CreateSingltone ();

		Instantiate (globalGameObject).GetComponent<AudioSource>().clip=clip;

	}

	private static void CreateSingltone(){
		GameObject template = new GameObject ("GlobalAudioSingletone");

		globalGameObject = template;//Instantiate (template);
		globalGameObject.AddComponent<AudioSource>();
		globalGameObject.AddComponent<GlobalAudio> ();

		globalGameObject.GetComponent<AudioSource> ().playOnAwake = false;
		globalGameObject.GetComponent<AudioSource> ().spatialBlend = 0.0f;
	}
}
