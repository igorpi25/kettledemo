﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class SetTrigger : MonoBehaviour {

    Animator animator;

    public TriggerParams[] triggers;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setTrigger(string triggerName){
        animator.SetTrigger(triggerName);
    }

    public void setBoolFalse(string paramName)
    {
        animator.SetBool(paramName, false);
    }

    public void setBoolTrue(string paramName)
    {
        animator.SetBool(paramName, true);
    }

    public void resetInteger(string paramName)
    {
        animator.SetInteger(paramName, 0);
    }

    public void setTriggerDelayed(int index)
    {
        StartCoroutine(setTriggerDelayEnumerator("sdfff", 0, 1));
    }

    public void setTriggerDelayed(string triggerName, float delay, float randomDelaySuffix)
    {
        StartCoroutine(setTriggerDelayEnumerator(triggerName, delay,randomDelaySuffix));
    }

    public void particleStart(GameObject objectWithParticle){

        Instantiate(objectWithParticle).GetComponent<ParticleSystem>().Play();

    }

    IEnumerator setTriggerDelayEnumerator(string triggerName, float delay, float randomDelaySuffix)
    {

        yield return new WaitForSeconds(delay + Random.Range(0, randomDelaySuffix));

        animator.SetTrigger(triggerName);
    }

    [System.Serializable]
    public class TriggerParams:System.Object{
        public string triggerName;
        public float delay=0.0f;
        public float suffix=0.0f;
    }
}
