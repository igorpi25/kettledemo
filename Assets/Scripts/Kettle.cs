﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kettle : MonoBehaviour {

    Animator animator;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void whenStartMove(){
        StartCoroutine(setTriggerAfterDelay("LidJump",0.5f));
        StartCoroutine(setTriggerAfterDelay("CapRotate", 0.5f));
    }

    IEnumerator setTriggerAfterDelay(string triggerName, float delay=0, float randomDelaySuffix=0)
    {

        yield return new WaitForSeconds(delay+Random.Range(0,randomDelaySuffix));

        animator.SetTrigger(triggerName);
    }

}
