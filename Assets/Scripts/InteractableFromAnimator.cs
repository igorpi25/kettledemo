﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableFromAnimator : MonoBehaviour {

    private bool active = true;
    public bool startTrigger=false;
    public string triggerName;

    private Animator animator;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}

    public void Update()
    {
        if (startTrigger && active)
        {
            animator.SetTrigger(triggerName);
            active = false;
        }
    }
	
}
