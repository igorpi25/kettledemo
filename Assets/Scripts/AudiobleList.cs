﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudiobleList : MonoBehaviour
{
    public bool dontPlayOnClick=false;

    public bool random = false;
    public float clickSpeed = 1;

    public int currentClipIndex = 0;
    public AudioClip[] list;

    AudioSource audiosource;

    // Use this for initialization
    void Start()
    {
        audiosource = GetComponent<AudioSource>();
    }


	void OnClicked(){
        if (!dontPlayOnClick)
        {
            PlaySound();
        }
    
	}

	public void PlaySound(){
		if ((list == null) || (list.Length == 0)) return;

		if (random) {
			currentClipIndex = Random.Range (0, list.Length);

		} else {
			currentClipIndex++;

			if (currentClipIndex >= list.Length)
				currentClipIndex = 0;
		}

		tag="Untagged";
		StartCoroutine (MakeClickableDelay());

		GlobalAudio.PlayOneShot(list[currentClipIndex]);
        


	}

    IEnumerator MakeClickableDelay()
    {
        yield return new WaitForSeconds(clickSpeed);
        tag = "clickable";
    }

}
