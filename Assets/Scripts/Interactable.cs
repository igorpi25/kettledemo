﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {

    Animator animator;
    public AudioSource audio;

    public GameObject targetAnimator=null;
    public string triggerName;


    public float clickSpeed = 0.5f;

    void Start() {

        if(targetAnimator != null)animator = targetAnimator.GetComponent<Animator>();
        else animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update() {



    }

    public void OnClicked()
    {



        if (triggerName != null) animator.SetTrigger(triggerName);
        if (audio != null) audio.Play();

        tag = "Untagged";

        if (clickSpeed <= 0.001f) return;
        else StartCoroutine(MakeClickableDelay());


    }

    IEnumerator MakeClickableDelay() {

        yield return new WaitForSeconds(clickSpeed);
        tag = "clickable";
    }

    
}


