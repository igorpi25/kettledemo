﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClick : MonoBehaviour {


	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonDown (0)) {
			RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);

            try
            {
                if ((hit.collider != null) && (hit.transform.gameObject.tag == "clickable"))
                {
                    hit.transform.gameObject.SendMessage("OnClicked");
                    return;
                }
            }
            catch (UnityException e)
            {
                Debug.Log("name of hit object: " + hit.transform.gameObject.name);
            }
        }

		if ((Input.touchCount>0) && (Input.GetTouch(0).phase==TouchPhase.Began)) {
			RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.GetTouch(0).position), Vector2.zero);

            Debug.Log("name of hit object(touch): " + hit.transform.gameObject.name);

            try
            {
                if ((hit.collider != null) && (hit.transform.gameObject.tag == "clickable"))
                {
                    hit.transform.gameObject.SendMessage("OnClicked");
                    return;
                }
            }
            catch(UnityException e){
                Debug.Log("e: " + e.Message);
                Debug.Log("tag of hit object(touch): " + hit.transform.gameObject.tag);
                Debug.Log("name of hit object(touch): "+ hit.transform.gameObject.name);
            }

			
		}
	}

}
