﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {

            if (SceneManager.GetActiveScene().name.Equals("MainScene"))
                Application.Quit();
            else MainMenu();
        }
    }

    public void GoToK(){
		GameObject canvas = GameObject.Find ("Canvas");
		canvas.SetActive (false);

		SceneManager.LoadScene ("SceneK");
	}

    public void GoToO()
    {
        GameObject canvas = GameObject.Find("Canvas");
        canvas.SetActive(false);

        SceneManager.LoadScene("SceneO");
    }

    public void MainMenu(){
		GameObject canvas = GameObject.Find ("Canvas");
		canvas.SetActive (false);

		SceneManager.LoadScene("MainScene");
	}

}
